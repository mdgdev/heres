# Heres I
Project Overview

[[_TOC_]]


## 1. Propósito general 

El Proyecto Heres I surge como solución a la necesidad de muchos profesionales de la salud mental de disponer de un sistema de tracking  y análisis de datos relacionados con la evolución de la terapia en sus pacientes. 

El objetivo de esta plataforma será la de proporcionar un entorno sencillo para que los pacientes puedan suministrar información sobre indicadores definidos como relevantes en su terapia. 

Estos datos, una vez almacenados, serán tratados de forma autónoma por un sistema de evaluación que servirá los datos procesados a los profesionales que lo requieran en el desarrollo de la terapia.

Las primeras fases de este proyecto se van a concentrar en la definición y estructura básica del sistema, la selección de los tratamientos y los parámetros de evaluación asociados y el sistema de evaluación integrado. 

## 2. Alternativas
Una rápida revisión del mercado nos arroja distintos tipos de plataformas que pueden realizar actividades similares a lo que pretende ser Heres:
### 2.1	Daylio – Diary, Journal, Mood Tracker
Aplicación de monitorización de estado de ánimo. 

#### Diferencias:
- Sólo permite un reducido rango de emociones y asociarlas a qué tipo de actividad estamos realizando. 
- Tiene muy poca utilidad terapéutica más allá de la vinculación acción – emoción y de un posible análisis de la tendencia emocional. 
  
#### A tener en cuenta: 
- Uso de emoticonos accesibles para cualquier usuario.
- Facilidad de manejo
- Empleo de los colores para definir emociones. 

### 2.2	MindDoc: Mood Tracker for Depression & Anxiety
Aplicación de monitorización y tratamiento para la depresión y la ansiedad.

#### Diferencias:
- Específica de los dos trastornos.
- Enfocada a ejercicios terapéuticos 
- Guía terapéutica

#### A tener en cuenta: 
- Empleo de respuestas prototípicas de tests relacionadas con la situación emocional del paciente. 
  - Podemos coger algunas ideas de la presentación de estas frases para integrarlas en Heres. 
  
### 2.3	Bearable - Symptoms & Mood tracker
Aplicación de monitorización completa de emociones y sintomatología asociada a distintos trastornos emocionales y de la conducta. 

#### Diferencias:
- Muy enfocado al usuario: muestra de objetivos y resultados en tiempo real (quizá sea interesante para mantener el engagement del usuario con la plataforma) 
- Preguntas muy simples con poca capacidad de extracción de resultados. 

##### A tener en cuenta: 
- Asociación entre sintomatología emocional y física. 
- Forma de presentar el estado emocional mediante colores y formas sencillas proporcionando mejor accesibilidad al usuario. 

### 2.4	Otras aplicaciones
La mayoría de las aplicaciones del mercado, especialmente las dirigidas a plataformas móviles, se enfocan en dos áreas: el registro emocional simple (estado de ánimo) y la terapia activa con tareas enfocadas al paciente. 

## 3. Resumen de funcionamiento
### 3.1 Usuario/Paciente
El usuario accede a la plataforma desde la URL proporcionada por el profesional / usando un código QR.

Una vez identificado el usuario es guiado a través de un sistema de evaluación en el que se le solicita información relevante acerca de su terapia. 

Una vez finalizado el test se le muestra la información que el profesional considera adecuada para que haya una percepción de progreso terapéutico. 

Una vez concluido el usuario recibe algún tipo de gratificación/refuerzo positivo que pueda aumentar la adhesión al tratamiento/terapia. (Sistema de fichas?) 

### 3.2 Usuario/Profesional
El profesional accede a un panel de control desde el que puede gestionar todos sus pacientes, generar informes o cambiar metodología terapéutica. 

Los informes son mostrados de forma gráfica permitiendo evaluar tendencias y evolución del tratamiento. 

### 3.3 Heres Core 
Sistema de evaluación automatizado que almacena los resultados obtenidos y elabora informes de situación y proyecciones de resultados en función de los distintos análisis de tendencia. 
Muestra además posibles alarmas/incidencias relacionadas con la salud mental del paciente y puede lanzar alertas en tiempo real. 

## 4. Fases de desarrollo 
_En construcción_ 

### 4.1 Fase 1
- Definición de Base de Datos de usuarios/profesionales/pacientes
- Definición de Base de Datos de Paciente 
- Sólo va a haber un profesional. 
- Login usuario/paciente
- Login usuario/profesional 
- Panel de control/profesional 
- Página de bienvenida/paciente 

|Nombre   | Rol  | Descripción 
|---|---|---
| Login Usuario  | Paciente   | Ventana de Login para usuario  
| Login Usuario   | Profesional   |  Ventana de Login para usuario  
| Landing Page  | Paciente  | Menú principal para el paciente una vez identificado.
| Dashboard | Profesional | Panel de control del profesional 

#### Front-end [Aura]
- Main Page
- Login Page
- Landing Page (Paciente)
- Dashboard (Profesional)

#### Back-end [Eos]
- Definición de Base de Datos 
- Gestion de objeto Paciente
  - Definición de atributos en BB.DD.
  - Definición de funciones de acceso.
  - Definición de funciones de modificación.
- Gestión de objeto Profesional
  - Definición de atributos en Base de Datos
  - Definición de funciones de acceso. 

### 4.2 Fase 2
#### Formularios de usuario (Therapy Based Forms [TBF])
Plantillas terapéuticas
- Atributos/parámetros de evaluación de la terapia
  - Preguntas | Tipo de Respuesta | Parámetros de evaluación. 

#### Tracing System Version 1
Sistema autónomo de evaluación de pacientes.
Terapias a analizar: 
- Depresión 
- Anisedad
- TOC ? 
- Inestabilidad emocional 

## 5. Flujo de acceso
Para la fase 1 del proyecto el flujo de acceso a la plataforma es el siguiente. 
![](flow.png)

## 6. Roles
### 6.1 Rol Usuario / Paciente
El paciente, identificado una vez se ha logado usando su nombre de usuario y contraseña es redirigido a su Landing Page (TBD) en la que tendrá una gestión simple de los elementos relacionados con la plataforma: Mensajes 
- Log diario de emociones
- Consultas 

### 6.2	Rol Usuario / Profesional
El profesional dispondrá de un panel de gestión Dashboard (TBD) en el que tendrá acceso a los distintos Pacientes (información general, informes y análisis gráfico), la definición de nuevas terapias/TBF,

### 6.3	Rol Administrador
Usuario con acceso a todos los usuarios para tareas de administración y gestión de cambios. 

## 7.  Requisitos funcionales 
### 7.1	Main Page
Breve información acerca de la plataforma y las soluciones que proporciona

### 7.2	Login Page

Login de usuario

### 7.3	Landing Page
- Perfil de usuario 
  - Consulta de datos de usuario
  - Modificación de datos de usuario 
- Log Diario 
  - Iniciar nuevo
  - Recuperar previo 
  - Borrar 
- Bandeja de mensajes
  - Consultar  
  - Borrar
- Consultas
  - Crear 

### 7.4	Dashboard
- Perfil de profesional
  - Consultar perfil del profesional
  - Modificar datos del profesional
- Lista de pacientes 
  - Acceso directo a panel de Paciente
- Panel de paciente
  - Tendencia
  - Informes
- Terapias
  - Crear
  - Modificar
  - Borrar
  - 
## 8. Evolución

Pasos a definir en siguientes iteraciones del proyecto. 
- Múltiples profesionales 
- Formularios de entrada 
- Entradas
- Diarios
- Terapias 
