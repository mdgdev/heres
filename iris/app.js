const config = require('./utils/config')
const express = require('express')
require('express-async-errors')
const app = express()
const cors = require('cors')
const trastornosRouter = require('./controllers/trastornos')
const categoriasRouter = require('./controllers/categorias')
const usuariosRouter = require('./controllers/usuarios')
const middleware = require('./utils/middleware')
const logger = require('./utils/logger')
const mongoose = require('mongoose') 

logger.info('connecting to', config.MONGODB_URI)

// Conexión con base de datos Atlas
mongoose.set('useFindAndModify', false);
mongoose.connect(config.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true })
    .then( () => {
        logger.info('connected to MongoDB')
    })
    .catch((error) => {
        logger.error('error connection to MongoDB:', error.message)
    })


app.use(cors())
app.use(express.static('build'))
app.use(express.json())
app.use(middleware.requestLogger)
app.use(middleware.tokenExtractor)

// API
app.use('/api/usuarios', usuariosRouter)
app.use('/api/dsm', trastornosRouter)
app.use('/api/categorias', categoriasRouter) 



// Definiciones de Middleware
app.use(middleware.unknownEndpoint)
app.use(middleware.errorHandler)

// Definiciones de GET
app.get('/', (req,res) => {
    res.send('Hello world');
})

app.get('/trastornos', (req,res) => {
    const trastornos = ["Trast1", "trast2"];
    res.send({
        trastornos: trastornos,
    });
});

module.exports = app