POST http://localhost:3001/api/dsm/
Content-Type: application/json

{
    "Categoria": "Categoría de test 2",
    "Tipo": "Tipo de test 2",
    "Codigo": "123",
    "Indice": "1112",
    "Trastorno": "Trastorno de test #1.",
    "Descripcion": "Descripcion del test",
    "Tecnicas": [
        "Test1",
        "Test2"
    ],
    "Criterios": [
        {
            "descripcion": "Descripcion 1",
            "subcriterios": "Subcriterios 1"
        },
        {
            "descripcion": "Descripcion 2"
        }
    ]
}
