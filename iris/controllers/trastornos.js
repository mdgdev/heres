const { Router, response } = require('express')
const trastornosRouter = Router()
// const jwt = require('jsonwebtoken')
const checkAuth = require('../middleware/check-auth');


const Trastorno = require('../models/trastorno')


trastornosRouter.get('/', async (req,res)=>{
    const trastornos = await Trastorno.find({}).sort({Order: 1})
    res.json(trastornos)
})


trastornosRouter.get('/:id', async (req,res)=>{
    const trastorno = await Trastorno.findById(req.params.id)
    res.json(trastorno)
})


trastornosRouter.post('/', checkAuth, async (req,res) => {
    const body = req.body 
    /* const decodedToken = jwt.verify(req.token, process.env.SECRET)
    if(!req.token || !decodedToken.id) {
        return response.status(401).json({ error: 'token missing or invalid' })
    }
    const user = await User.findById(decodedToken.id) */

    const trastorno = new Trastorno({
        Categoria: body.Categoria,
        Tipo: body.Tipo,
        Codigo: body.Codigo,
        Indice: body.Indice,
        Trastorno: body.Trastorno,
        Descripcion: body.Descripcion,
        Criterios: body.Criterios,
        Tecnicas: body.Tecnicas, 
    })

    const savedTrastorno = await trastorno.save();
    res.status(201).json(savedTrastorno);

})

/* trastornosRouter.get('/set/order', async (req,res) => {
    
    const all = await Trastorno.find({})
    let i = 0;
    all.map ( async trastorno => { 
        let valor = i.toString(); 
        trastorno.Order = i; 
        i = i + 1;
        console.log(i)
        console.log(valor)
        await Trastorno.findByIdAndUpdate(trastorno.id, trastorno )
       

    })

    // const updated = await Trastorno.updateOne({}, {"$set":{"Order": "1"}});
    

})  */



trastornosRouter.put('/:id', checkAuth, async (req,res) => {
    
    const body = req.body; 

    const trastorno = {
        Categoria: body.Categoria,
        Tipo: body.Tipo,
        Codigo: body.Codigo,
        Indice: body.Indice,
        Trastorno: body.Trastorno,
        Descripcion: body.Descripcion,
        Criterios: body.Criterios,
        Tecnicas: body.Tecnicas, 
    }
   

    const updatedTrastorno = await Trastorno.findByIdAndUpdate(req.params.id, trastorno, {new: true});
    res.json(updatedTrastorno); 
})


module.exports = trastornosRouter