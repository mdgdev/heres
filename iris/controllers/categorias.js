const { Router, response } = require('express')
const categoriasRouter = Router()
const jwt = require('jsonwebtoken')

const Categoria = require('../models/categoria');

categoriasRouter.get('/', async (req,res) => {
    const categorias = await Categoria.find({});
    res.json(categorias)
})

module.exports = categoriasRouter;