const { Router, response } = require("express");
const usuariosRouter = Router();
const checkAuth = require('../middleware/check-auth');

const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const Usuario = require("../models/usuario");


usuariosRouter.post("/signup", async (req, res, next) => {
  const user = await Usuario.find({ email: req.body.email });
  console.log('user',user)
  if (user && user.size > 0) {
    return res.status(409).json({
      message: "Mail ya existe",
    });
  } else {
    bcrypt.hash(req.body.password, 10, async (err, hash) => {
      if (err) {
        return res.status(500).json({
          error: err,
        });
      } else {
        try {
          const user = new Usuario({
            nombre: req.body.nombre,
            email: req.body.email,
            tipo: "usuario",
            password: hash,
          });

          const result = await user.save();
          res.status(201).json({
            message: "User created",
          });
        } catch (err) {
          console.log(err);
          res.status(500).json({
            error: err,
          });
        }
      }
    });
  }
});

usuariosRouter.delete('/:userId', async (req, res) => {
    try{ 
        const result = await Usuario.deleteOne({ id: req.params.userId }) 
        console.log(result);
        res.status(201).json({
            message: "User removed"
        })
        }catch(err){
            console.log(err)
            res.status(500).json({
                error: err} );

        }
    
})

usuariosRouter.post('/login', async (req, res) => {
    
    try{
        const user = await Usuario.find({
            email: req.body.email 
        })
        if (user.size < 1){
            return res.status(404).json({
                message: 'Auth error'
            }) 
        }
        bcrypt.compare(req.body.password, user[0].password, (err, result) => {
            if (err) {
                return res.status(401).json({
                    message: 'Auth failed'
                });
            }
            if (result) {
               const token = jwt.sign({
                    email: user[0].email,
                    userId: user[0].id
                }, process.env.JWT_KEY, 
                {
                    expiresIn: "1h"
                },
                )
                return res.status(200).json({
                    message: 'Auth successful',
                    token: token,
                })
            }
            res.status(401).json({
                message: 'Auth failed'
            });
        });
    }catch(err){
        console.log(err)
        res.status(501).json({
            error: err
        })
    }

})

module.exports = usuariosRouter;
