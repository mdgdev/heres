const mongoose = require('mongoose')

mongoose.set('useFindAndModify', false);

const categoriaSchema = new mongoose.Schema({
    Nombre: { type: String, required: true},
    Tipos: { type: Array}
});

categoriaSchema.set('toJSON', {
    transform: (document, returnedObject) => {
        returnedObject.id = returnedObject._id.toString()
        delete returnedObject._id
        delete returnedObject.__v
    }
})


module.exports = mongoose.model('Categoria', categoriaSchema)