const mongoose = require('mongoose')

mongoose.set('useFindAndModify', false)

const trastornoSchema = new mongoose.Schema({
    Categoria: { type: String, required: true },
    Tipo: String,
    Codigo: String,
    Indice: String,
    Trastorno: { type: String, required: true },
    Descripcion: String,
    Criterios: { type: Array },
    Tecnicas: { type: Array },
    Order: Number,
});

trastornoSchema.set('toJSON', {
    transform: (document, returnedObject) => {
        returnedObject.id = returnedObject._id.toString()
        delete returnedObject._id
        delete returnedObject.__v
    }
})

module.exports = mongoose.model('Trastorno', trastornoSchema)