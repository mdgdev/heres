import jwt from 'jsonwebtoken'; 
import bcrypt from 'bcrypt';
import express from 'express';
import User from '../models/users';
import { Usuario } from '../models/users';

require('dotenv').config();

const loginRouter = express.Router()


loginRouter.post('/', async (req, res) => {
  const body = req.body

  try{ 
  
  const user = await User.findOne({ username: body.username })
  const passwordCorrect = user === null
    ? false
    : await bcrypt.compare(body.password, user.passwordHash)

  if (!(user && passwordCorrect)) {
    return res.status(401).json({
      error: 'invalid username or password'
    })
  }

  const userForToken = {
    username: user.username,
    id: user._id
  }

  if(process.env.SECRET) {
  const token = jwt.sign(userForToken, process.env.SECRET)

  res
    .status(200)
    .send({ token, username: user.username, name: user.name })
  }

}
catch(error){
  console.log("Error:", error);
}
});

export default loginRouter; 