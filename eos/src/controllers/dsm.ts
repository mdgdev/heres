import express from "express";
import { dsm } from "../data/dsm5";
import { trastornos } from '../data/trastornos';

// API Básico de DSM-5


interface Trastorno {
  Id: number;
  Codigo: string;
  Trastorno: string;
  Indice: string;
  Descripcion?: string;
  Criterios?: Criterio[];
  Categoria: string;
  Tipo: string;
}

interface Criterio { 
  Descripcion: string;
  Subcriterios?: string[];
}

interface Especifica {
  nombre: string;
  descripcion: string; 
}

const dsmRouter = express.Router();

dsmRouter.get("/", (_req, res) => {
  res.json(trastornos);
});

dsmRouter.get("/:id", (req, res) => {
// Gestor de trastornos
let found = 0;
trastornos.map( trastorno => {
  if ( trastorno.Id.toString() === req.params.id) {
    res.json(trastorno);
    found = 1;
  }
})
if (found === 0)
res.json("Not found");
});

export default dsmRouter;