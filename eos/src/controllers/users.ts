import bcrypt from 'bcrypt';
import express from 'express';

import User from '../models/users';
const userRouter = express.Router();

// -- Crear usuarios -- 
userRouter.post('/', async (req,res) => {
    const body = req.body;

    const user_exists = await User.find({ username: body.username })

    if(!body.username || !body.password ){
        res.status(400).json({
            error: 'Missing username or password'
        }).end();
    }else if(body.username.length < 3 || body.password.length <3) {
        res.status(400).json({
            error: 'Username and password must have al least 3 charaters'
        }).end()
    }else if (user_exists.length > 0 ){
        res.status(400).json({
            error: 'Username already exists'
        }).end()
    } else {
        const saltRounds = 10
        const passwordHash = await bcrypt.hash(body.password, saltRounds)

        const user = new User({
            username: body.username,
            name: body.name,
            surname1: body.surname1,
            surname2: body.surname2 ? body.surname2 : null,
            gender: body.gender,
            birthDate: body.birthDate,
            type: body.type,
            passwordHash,
        })
       
        const savedUser = await user.save()
        res.status(201).json(savedUser)
    }
});

export default userRouter;



