export const dsm = [
  {
    categoria: "Trastornos del desarrollo neurológico",
    tipos: [
      {
        tipo: "Discpacidades intelectuales",
        trastornos: [
          {
            codigo: "F70",
            trastorno: "Discapacidad intelectual leve",
            indice: "317",
            descripcion:
              "La discapacidad intelectual (trastorno del desarrollo intelectual) es un trastorno que comienza durante el período de desarrollo y que incluye limitaciones del funcionamiento intelectual como también del comportamiento adaptativo en los dominios conceptual, social y práctico.",
            criterios: [
              {
                descripcion:
                  "Deficiencias de las funciones intelectuales, como el razonamiento, la resolución de problemas, la planificación, el pensamiento abstracto, el juicio, el aprendizaje académico y el aprendizaje a partir de la experiencia, confirmados mediante la evaluación clínica y pruebas de inteligencia estandarizadas individualizadas.",
              },
              {
                descripcion:
                  "Deficiencias del comportamiento adaptativo que producen fracaso del cumplimiento de los estándares de desarrollo y socioculturales para la autonomía personal y la responsabilidad social. Sin apoyo continuo, las deficiencias adaptativas limitan el funcionamiento en una o más actividades de la vida cotidiana, como la comunicación, la participación social y la vida independiente en múltiples entornos, tales como el hogar, la escuela, el trabajo y la comunidad.",
              },
              {
                descripcion:
                  "Inicio de las deficiencias intelectuales y adaptativas durante el período de desarrollo.",
              },
            ],
          },
          {
            codigo: "F71",
            trastorno: "Discapacidad intelectual moderada",
            indice: "318",
            descripcion:
              "La discapacidad intelectual (trastorno del desarrollo intelectual) es un trastorno que comienza durante el período de desarrollo y que incluye limitaciones del funcionamiento intelectual como también del comportamiento adaptativo en los dominios conceptual, social y práctico.",
            criterios: [
              {
                descripcion:
                  "Deficiencias de las funciones intelectuales, como el razonamiento, la resolución de problemas, la planificación, el pensamiento abstracto, el juicio, el aprendizaje académico y el aprendizaje a partir de la experiencia, confirmados mediante la evaluación clínica y pruebas de inteligencia estandarizadas individualizadas.",
              },
              {
                descripcion:
                  "Deficiencias del comportamiento adaptativo que producen fracaso del cumplimiento de los estándares de desarrollo y socioculturales para la autonomía personal y la responsabilidad social. Sin apoyo continuo, las deficiencias adaptativas limitan el funcionamiento en una o más actividades de la vida cotidiana, como la comunicación, la participación social y la vida independiente en múltiples entornos, tales como el hogar, la escuela, el trabajo y la comunidad.",
              },
              {
                descripcion:
                  "Inicio de las deficiencias intelectuales y adaptativas durante el período de desarrollo.",
              },
            ],
          },
          {
            codigo: "F72",
            trastorno: "Discapacidad intelectual grave",
            indice: "318.1",
            descripcion:
              "La discapacidad intelectual (trastorno del desarrollo intelectual) es un trastorno que comienza durante el período de desarrollo y que incluye limitaciones del funcionamiento intelectual como también del comportamiento adaptativo en los dominios conceptual, social y práctico.",
            criterios: [
              {
                descripcion:
                  "Deficiencias de las funciones intelectuales, como el razonamiento, la resolución de problemas, la planificación, el pensamiento abstracto, el juicio, el aprendizaje académico y el aprendizaje a partir de la experiencia, confirmados mediante la evaluación clínica y pruebas de inteligencia estandarizadas individualizadas.",
              },
              {
                descripcion:
                  "Deficiencias del comportamiento adaptativo que producen fracaso del cumplimiento de los estándares de desarrollo y socioculturales para la autonomía personal y la responsabilidad social. Sin apoyo continuo, las deficiencias adaptativas limitan el funcionamiento en una o más actividades de la vida cotidiana, como la comunicación, la participación social y la vida independiente en múltiples entornos, tales como el hogar, la escuela, el trabajo y la comunidad.",
              },
              {
                descripcion:
                  "Inicio de las deficiencias intelectuales y adaptativas durante el período de desarrollo.",
              },
            ],
          },
          {
            codigo: "F73",
            trastorno: "Discapacidad intelectual profunda",
            indice: "318.2",
            descripcion:
              "La discapacidad intelectual (trastorno del desarrollo intelectual) es un trastorno que comienza durante el período de desarrollo y que incluye limitaciones del funcionamiento intelectual como también del comportamiento adaptativo en los dominios conceptual, social y práctico.",
            criterios: [
              {
                descripcion:
                  "Deficiencias de las funciones intelectuales, como el razonamiento, la resolución de problemas, la planificación, el pensamiento abstracto, el juicio, el aprendizaje académico y el aprendizaje a partir de la experiencia, confirmados mediante la evaluación clínica y pruebas de inteligencia estandarizadas individualizadas.",
              },
              {
                descripcion:
                  "Deficiencias del comportamiento adaptativo que producen fracaso del cumplimiento de los estándares de desarrollo y socioculturales para la autonomía personal y la responsabilidad social. Sin apoyo continuo, las deficiencias adaptativas limitan el funcionamiento en una o más actividades de la vida cotidiana, como la comunicación, la participación social y la vida independiente en múltiples entornos, tales como el hogar, la escuela, el trabajo y la comunidad.",
              },
              {
                descripcion:
                  "Inicio de las deficiencias intelectuales y adaptativas durante el período de desarrollo.",
              },
            ],
          },
          {
            codigo: "F79",
            trastorno: "Discapacidad intelectual de gravedad no especificada",
            indice: "319",
            descripcion: "test",
            criterios: [
              {
                descripcion:
                  "Deficiencias de las funciones intelectuales, como el razonamiento, la resolución de problemas, la planificación, el pensamiento abstracto, el juicio, el aprendizaje académico y el aprendizaje a partir de la experiencia, confirmados mediante la evaluación clínica y pruebas de inteligencia estandarizadas individualizadas.",
              },
              {
                descripcion:
                  "Deficiencias del comportamiento adaptativo que producen fracaso del cumplimiento de los estándares de desarrollo y socioculturales para la autonomía personal y la responsabilidad social. Sin apoyo continuo, las deficiencias adaptativas limitan el funcionamiento en una o más actividades de la vida cotidiana, como la comunicación, la participación social y la vida independiente en múltiples entornos, tales como el hogar, la escuela, el trabajo y la comunidad.",
              },
              {
                descripcion:
                  "Inicio de las deficiencias intelectuales y adaptativas durante el período de desarrollo.",
              },
            ],
          },
          {
            codigo: "F88",
            trastorno: "Retraso global del desarrollo",
            indice: "315.8",
            descripcion:
              "Este diagnóstico se reserva para individuos menores de 5 años cuando el nivel de gravedad clínica no se puede valorar de forma fiable durante los primeros años de la infancia. Esta categoría se diagnostica cuando el sujeto no cumple con los hitos del desarrollo esperados en varios campos del funcionamiento intelectual y se aplica a los individuos en que no se puede llevar a cabo una valoración sistemática del funcionamiento intelectual, incluidos los niños demasiado pequeños para participar en pruebas estandarizadas. Esta categoría se debe volver a valorar después de un período de tiempo.",
            criterios: [
              {
                descripcion: "Definir",
              },
            ],
          },
          {
            codigo: "F79",
            trastorno:
              "Discapacidad intelectual (trastorno del desarrollo intelectual) no especificada",
            indice: "N/D",
            descripcion:
              "Esta categoría se reserva para individuos mayores de 5 años cuando la valoración del grado de discapacidad intelectual (trastorno del desarrollo intelectual) es difícil o imposible mediante los procedimientos localmente disponibles debido a deterioros sensoriales o físicos asociados, como la ceguera o la sordera prelingual y la discapacidad locomotora, a la presencia de problemas de comportamiento graves o a la existencia concurrente de un trastorno mental. Esta categoría sólo se utilizará en circunstancias excepcionales y se debe volver a valorar después de un período de tiempo.",
            criterios: [
              {
                descripcion: "Definir",
              },
            ],
          },
        ],
      },
      {
        tipo: "Trastornos de la comunicación",
        trastornos: [
          {
            codigo: "F80.2",
            trastorno: "Trastorno del Lenguaje",
            indice: "315.32",
            descripcion: "N/D",
            criterios: [
              {
                descripcion:
                  "Dificultades persistentes en la adquisición y uso del lenguaje en todas sus modalidades (es decir, hablado, escrito, lenguaje de signos u otro) debido a deficiencias de la comprensión o la producción que incluye lo siguiente:",
                  subcriterios: [
                    "Vocabulario reducido (conocimiento y uso de palabras)",
                    "Estructura gramatical limitada (capacidad para situar las palabras y las terminaciones de palabras juntas para formar frases basándose en reglas gramaticales y morfológicas).",
                    "Deterioro del discurso (capacidad para usar vocabulario y conectar frases para explicar describir un tema o una serie de sucesos o tener una conversación).",
                  ],
              },
            ],
          },
          {
            codigo: "F80.0",
            trastorno: "Trastorno fonológico",
            indice: "315.39",
            descripcion: "",
            criterios: [
              {
                descripcion: "",
                  subcriterios: [],
              },
            ],
          },
          {
            codigo: "F80.81",
            trastorno: "Trastorno de fluidez (tartamudeo) de inicio en la infancia",
            indice: "315.35",
            descripcion: "",
            criterios: [
              {
                descripcion: "",
                  subcriterios: [],
              },
            ],
          },
          {
            codigo: "F80.89",
            trastorno: "Trastorno de la comunicación social (pragmático)",
            indice: "315.39",
            descripcion: "",
            criterios: [
              {
                descripcion: "",
                  subcriterios: [],
              },
            ],
          },
          {
            codigo: "F80.9",
            trastorno: "Trastorno de la comunicación no especificado",
            indice: "307.9",
            descripcion: "",
            criterios: [
              {
                descripcion: "",
                  subcriterios: [],
              },
            ],
          },
        ],
      },
      {
        tipo: "Trastorno del espectro del autismo",
        trastornos: [
          {
            codigo: "F84.0",
            trastorno: "Trastorno del espectro del autismo",
            indice: "",
            descripcion: "",
            criterios: [
              {
                descripcion: "",
                  subcriterios: [],
              },
            ],
          },
        ]
      },
      {
        tipo: "Trastornos del aprendizaje",
        trastornos: [
          {
            codigo: "F81.0",
            trastorno: "Trastorno de la lectura",
            indice: "315.00",
            descripcion: "",
            criterios: [],
          },
          {
            codigo: "F81.1",
            trastorno: "Trastorno del cálculo",
            indice: "315.1",
            descripcion: "",
            criterios: [],
          },
          {
            codigo: "F81.2",
            trastorno: "Trastorno de la expresión escrita ",
            indice: "315.2",
            descripcion: "",
            criterios: [],
          },
          {
            codigo: "F81.3",
            trastorno: " Trastorno del aprendizaje no especificado",
            indice: "315.9",
            descripcion: "",
            criterios: [],
          },
        ],
      },
      {
        tipo: "Trastornos de las habilidades motoras",
        trastornos: [
          {
            codigo: "F82",
            trastorno: "Trastorno del desarrollo de la coordinación",
            indice: "315.4",
            descripcion: "",
            criterios: [],
          },
        ],
      },
      {
        tipo: "Trastornos del lenguaje",
        trastornos: [
          {
            codigo: "F80.1",
            trastorno: "Trastorno del lenguaje expresivo",
            indice: "315.31",
            descripcion: "",
            criterios: [],
          },
          {
            codigo: "F80.2",
            trastorno: "Trastorno mixto del lenguaje receptivo-expresivo",
            indice: "315.31",
            descripcion: "",
            criterios: [],
          },
          {
            codigo: "F80.0",
            trastorno: "Trastorno fonológico",
            indice: "315.39",
            descripcion: "",
            criterios: [],
          },
          {
            codigo: "F98.5",
            trastorno: "Tartamudeo",
            indice: "307.01",
            descripcion: "",
            criterios: [],
          },
          {
            codigo: "F80.9",
            trastorno: "Trastorno de la comunicación no especificado",
            indice: "307.9",
            descripcion: "",
            criterios: [],
          },
        ],
      },
      {
        tipo: "Trastoros generalizados del desarrollo",
        trastornos: [
          {
            codigo: "F84.0",
            trastorno: "Trastorno autista",
            indice: "299.00",
            descripcion: "",
            criterios: [],
          },
          {
            codigo: "F84.2",
            trastorno: "Trastorno de Rett",
            indice: "299.80",
            descripcion: "",
            criterios: [],
          },
          {
            codigo: "F84.3",
            trastorno: "Trastorno desintegrativo infantil",
            indice: "299.10",
            descripcion: "",
            criterios: [],
          },
          {
            codigo: "F84.5",
            trastorno: "Trastorno de Asperger",
            indice: "299.80",
            descripcion: "",
            criterios: [],
          },
          {
            codigo: "F84.9",
            trastorno: "Trastorno generalizado del desarrollo no especificado",
            indice: "299.80",
            descripcion: "",
            criterios: [],
          },
          {
            codigo: "F88.0",
            trastorno: "Retraso global del desarrollo",
            indice: "315.8",
            descripcion: "",
            criterios: [],
          },
        ],
      },
    ],
  },
  {
    categoria: "Espectro de la esquizofrenia",
    tipos: [
      {
        tipo: "Espectro de la esquizofrenia (General)",
        trastornos: [
          {
            codigo: "F20.9",
            trastorno: "Esquizofrenia",
            indice: "295.90",
            descripcion: "",
            criterios: [],
          },
          {
            codigo: "F21",
            trastorno: "Trastornoi de la personalizdad esquizotípico",
            indice: "301.22",
            descripcion: "",
            criterios: [],
          },
        ],
      },
      {
        tipo: "Trastornos psicóticos distintos de la esquizofrenia",
        trastornos: [
          {
            codigo: "F20.81",
            trastorno: "Trastorno esquizofreniforme",
            indice: "295.40",
            descripcion: "",
            criterios: [],
          },
        ],
      },
    ],
  },
];
