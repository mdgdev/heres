export enum Rol {
    Profesional = "profesional",
    Paciente = "paciente"
}

export enum Gender { 
    Masculino = "hombre",
    Femenino = "mujer"
}

export type Usuario = {
    username: string;
    email: string;
    name: string;
    surname1: string;
    surname2?: string;
    gender: Gender; 
    birthDate: string;
    type: Rol;
    passwordHash: string;
    // 1.0.1: Entradas[]
}

