import express from "express";
import config from './utils/config';
import cors from 'cors';
import loginRouter from './controllers/login';
import userRouter from './controllers/users';
import dsmRouter from './controllers/dsm';
import mongoose from 'mongoose';

const app = express();
const port = 8080; // default port to listen

app.use(cors());
app.use(express.static('build'));
app.use(express.json());

if (config.MONGODB_URI){ 
    mongoose.connect(config.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true})
}
    

app.use('/api/login', loginRouter);
app.use('/api/users', userRouter);
app.use('/api/dsm', dsmRouter);


// define a route handler for the default home page
app.get( "/", ( req, res ) => {
    res.send( "HERES-I / API 1.0" );
} );

// start the Express server
app.listen( port, () => {
    console.log( `server started at http://localhost:${port}` );
} );

