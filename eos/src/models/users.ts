import { Document, Model, model, Types, Schema, Query } from "mongoose"
import { Gender } from '../types/usuario';
import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

export interface Usuario extends mongoose.Document {
    username: string,
    name: string,
    surname1: string,
    surname2?: string,
    gender: Gender,
    birthDate: string,
    type: string,
    passwordHash: string,
}

const userSchema: Schema = new Schema({
    username: {type: String, unique: true },
    name: String,
    surname1: String,
    surname2: String,
    gender: String,
    birthDate: String,
    type: String,
    passwordHash: String,
});

userSchema.set('toJSON',{
    transform: (document, returnedObject) => {
        returnedObject.id = returnedObject._id.toString();
        delete returnedObject._id;
        delete returnedObject.__v;
        delete returnedObject.passwordHash;
    }
})


export default model<Usuario>('User', userSchema);