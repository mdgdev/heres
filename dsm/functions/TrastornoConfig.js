export const addSubCriterio = (
  target,
  i,
  j,
  trastornoCriterios,
  setTrastornoCriterios
) => {
    const newTrastornoCriterios = [...trastornoCriterios];

  if (newTrastornoCriterios[i].Subcriterios) {
    newTrastornoCriterios[i].Subcriterios[j] = target.value;
  } else {
    const newCriterio = {
      Descripcion: newTrastornoCriterios[i].Descripcion,
      Subcriterios: [target.value],
    };
    newTrastornoCriterios[i] = newCriterio;
  }
  setTrastornoCriterios(newTrastornoCriterios);
};


export const addTrastornoCriterio = (target, i, trastornoCriterios, setTrastornoCriterios) => {
    const newTrastornoCriterios = [...trastornoCriterios];
    const newCriterio = {...newTrastornoCriterios[i],
      Descripcion: target.value,
    };
    newTrastornoCriterios[i] = newCriterio;
    setTrastornoCriterios(newTrastornoCriterios);
  };


export default { addSubCriterio, addTrastornoCriterio };
