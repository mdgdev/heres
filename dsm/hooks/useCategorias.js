import { useState, useEffect } from "react";

const useCategorias = ([]) => {

const [categorias, setCategorias] = useState([]);

useEffect(() => {
    fetch(`http://localhost:3001/api/categorias/`)
      .then((response) => response.json())
      .then((data) => setCategorias(data));
  }, []);

  
  return categorias;

}

export default useCategorias; 