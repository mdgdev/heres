import { useState } from "react";

// Custom Hook (0.2.3)

const useTrastornos = (valor) => {
  const [trastornos, setTrastornos] = useState(valor);

  const changeTrastornos = (valor, parametro) => {
    const newTrastornos = trastornos
    newTrastornos[parametro] = valor
    setTrastornos(newTrastornos);
    
  }

  const updateTrastorno = (trastorno) => {
      setTrastornos(trastorno)
  }
return { trastornos, updateTrastorno, changeTrastornos };
};

export default useTrastornos;
