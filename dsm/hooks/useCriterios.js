import { useState } from 'react';

// Custom Hook 0.2.3 

const useCriterios = (valor) => {

    const [criterios, setCriterios] = useState(valor);
    

    const addCriterio = () => {
        const criteriosNumActual = criterios + 1;
        setCriterios(criteriosNumActual)
    }

    return { criterios, addCriterio };

}

export default useCriterios;