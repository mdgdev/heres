import { Button } from "@chakra-ui/react";
import { useState } from "react";

const PageButtons = ({ total, setPageNumber, pageNumber }) => {
  const [curr, setCurr] = useState(0);
  const [pos, setPos] = useState(0);
  const pages = Math.ceil((total / 8) - 1);
  const buttons = [];
  let prev = [];
  let next = [];

 
  
  if (curr > 0) {

    prev.push(
      <Button
        colorScheme="teal"
        mr={1}
        onClick={() => {
          const newCurr = curr - 6;
          if (newCurr >= 0) {
            setCurr(newCurr)
          }
        }}
      >
        Prev
      </Button>
    );

    prev.push(
<Button colorScheme="teal" mr={1} onClick={() =>{ 
            setPos(posicion)
            setCurr(0)
            setPageNumber(0)}}>
          0
        </Button>

    )
    
    prev.push(
      <Button colorScheme="teal" mr={1}>
        ...
      </Button>
    );
  }else{
    const newPrev = [];
    prev = [...newPrev]
  }

  let posicion = 0;
  for (let i = curr; i <= pages; i++) {
    
    if (i === pageNumber) {
      buttons.push(
        <Button colorScheme="blue" mr={1}>
          {i}
        </Button>
      );
    } else {
      buttons.push(
        <Button colorScheme="teal" mr={1} onClick={() =>{ 
            setPos(posicion)
            setPageNumber(i)}}>
          {i}
        </Button>
      );
    }
    posicion = posicion + 1;
  }

  const newButtons = buttons.slice(0,8);

  
  if (pageNumber < pages-4) {
    next.push(
      <Button colorScheme="teal" mr={1}>
        ...
      </Button>
    );
    next.push(
      <Button colorScheme="teal" mr={1} onClick={() =>{ 
        setPos(6)
        setCurr(pages-6)
        setPageNumber(pages)}}>
      {pages}
    </Button>
    );
  


  next.push(
    <Button
      colorScheme="teal"
      mr={1}
      onClick={() => {
        const newCurr = curr + 6;
        if(newCurr < pages){
          setCurr(newCurr);
        } 

      }}
    >
      Sig
    </Button>
  );

}else{
  next = [];
}

  return <>
  {prev}
  {newButtons}
  {next}</>;
};

export default PageButtons;
