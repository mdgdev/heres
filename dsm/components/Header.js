import { useState, useEffect } from 'react';
import { Box, Flex, Center, Button, Text, Square, Spacer, Image } from "@chakra-ui/react";
import Logo from '../recursos/logo.png'
import styles from './Header.module.css'
import CONST from '../resources/CONST'
import jwt_decode from 'jwt-decode';
import Cookie from 'js-cookie';

const Header = ({setPage, token, setToken}) => {
  const [email, setEmail] = useState();
  const [myToken, setMyToken] = useState(token);
  
  useEffect(()=> {
    if(token){
      const decoded = jwt_decode(token)
      setEmail(decoded.email)
    } 
    
  }, [token])
  

  const logOut = () => {
    Cookie.remove('token');
    setMyToken();
    setToken();
  }
 

  return (
    <>
      <Box bgColor="white" h="80px">
        <Flex h="80px">
          <Center w="200px">
            <Image src={Logo} alt="logo" />
          </Center>
          <Spacer />
          <Box  w="600px" >
            <Text className={styles.tituloHeader} align="right" pt="20px" pr="20px">Sistema de búsqueda automatizada del Diagnostic and Statistical Manual v5</Text>
            <Text className={styles.tituloSub} align="right" pr="20px">Version {CONST.version} - {CONST.iteration} (en pruebas)</Text>
          </Box>
          <Box 
          mt={5}
          mr={5}
          >
            {!myToken ? 
            <Button 
            colorScheme='teal'
            size='sm'
            onClick={() => setPage('Login')}
            >Login</Button> : 
            <Button
            onClick={logOut}
            >
              {email}</Button>}
          </Box> 

        </Flex>
      </Box>
    </>
  );
};

export default Header;
