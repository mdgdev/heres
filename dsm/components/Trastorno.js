import CONST from "../resources/CONST";
import { Box, Button, Badge, Text, useDisclosure } from "@chakra-ui/react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
} from "@chakra-ui/react";
import TrastornoDetail from "./TrastornoDetail";
import styles from "./Trastorno.module.css";

const Trastorno = (props) => {
  const { isOpen, onOpen, onClose } = useDisclosure();


  return (
    <>
      <Box
        backgroundColor="white"
        p="10px"
        align="center"
        borderRadius="10px"
        boxShadow="0px 0px 5px 2px #DDDDDD"
        w="100%"
      >
        <Box>
          <Badge colorScheme="green" fontSize="0.8em">
            {props.codigo}
          </Badge>
          &nbsp;
          <Badge colorScheme="blue" fontSize="0.8em">
            {props.indice}
          </Badge>
        </Box>
        <Box mt="1" fontWeight="semibold">
          <Text
            align="center"
            className={styles.nuevaFuente}
            mt="10px"
            mb="5px"
          >
            {props.trastorno}
          </Text>
        </Box>

        <Box>
          <Text className={styles.categoria}>{props.categoria}</Text>
        </Box>
        <Box>
          <Text className={styles.tipo}>
             {props.tipo}
          </Text>
        </Box>
        <Box pt="10px"><Button onClick={onOpen} size="sm">Información</Button>
            </Box>
      </Box>

      <Modal isOpen={isOpen} onClose={onClose} size="4xl">
        <ModalOverlay />
        <ModalContent>
          <ModalHeader fontSize="12px" textColor="gray.400">
            {CONST.name} {CONST.version} {CONST.iteration}
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <TrastornoDetail index={props.id} token={props.token} />
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};

export default Trastorno;
