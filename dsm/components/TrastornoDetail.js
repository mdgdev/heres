import {
  Box,
  Button,
  Badge,
  Table,
  TableCaption,
  Thead,
  Tr,
  Th,
  Td,
  Tbody,
  Text,
  Grid,
  GridItem,
  OrderedList,
  ListItem,
  Heading
} from "@chakra-ui/react";

import { useEffect, useState } from "react";
import styles from "./TrastornoDetail.module.css";
import TrastornoEdit from "../components/forms/TrastornoEdit";

const TrastornoDetail = (props) => {
  const [trastorno, setTrastorno] = useState({});
  const [edit, setEdit] = useState(false);
  const abc = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];

  useEffect(() => {
    fetch(`http://localhost:3001/api/dsm/${props.index}`)
      .then((response) => response.json())
      .then((data) => setTrastorno(data));
  }, []);

  return (
    <>
      <Box align="right">
        {props.token ? <Button colorScheme="blue" size="xs" onClick={() => setEdit(true)}>
          Editar
        </Button> : <></>}
      </Box>
      {!edit ? (
        <>
          <Badge>{trastorno.Categoria}</Badge>
          <div className={styles.TrastornoTitulo}>{trastorno.Trastorno}</div>
          <Box mt="5px" mb="20px" className={styles.descripcion}>
            {trastorno.Descripcion}
          </Box>

          {trastorno.Criterios && trastorno.Criterios.length ? (
            <>
            <Text className={styles.criteriosTitle}>Criterios</Text>
            <Table variant="simple">
              <TableCaption>
                Lista de criterios asociados a {trastorno.Trastorno}
              </TableCaption>

              {trastorno.Criterios.map((criterio, index) => {
                return (
                  <Tbody className={styles.criterio}>
                    <Tr>
                      <Box
                        borderWidth="1px"
                        borderRadius="lg"
                        borderColor="blue.200"
                        bg="blue.50"
                        mb={5}
                      >

                        <Td key={index}>
                        <Grid templateColumns="repeat(15, 1fr)">
                          <GridItem colSpan={1}>
                        <Text fontSize='3xl'mr={5} mb={5} mt={1}>{abc[index]}</Text>
                        </GridItem>
                        <GridItem colSpan={14}>
                        <Text textAlign='justify'>{criterio.Descripcion}</Text>

                          
                            
                              {criterio.Subcriterios && criterio.Subcriterios.length
                                ? criterio.Subcriterios.map(
                                    (subcriterio, index) => (
                                     <>
                                      <Box mt="10px" mb="10px" ml={5} bg='white' p={5}  borderWidth="1px"
                        borderRadius="xl" borderColor="blue.200">
                          <Text fontSize='xl' float='left' mr={5} mb={5} mt={1} >{index+1}</Text>
                                        <Text textAlign='justify'>{subcriterio}</Text>
                                      </Box>
                                      </>
                                    )
                                  )
                                : null}
                                </GridItem>
                        </Grid>
                        </Td>
                      </Box>
                    </Tr>
                  </Tbody>
                );
              })}
            </Table>
            </>
          ) : (
            <div></div>
          )}
          {trastorno.Tecnicas && trastorno.Tecnicas.length ? (
            <Table variant="simple">
              <TableCaption>
                Lista de técnicas para trabajar con {trastorno.Trastorno}
              </TableCaption>
              <Thead>
                <Tr>
                  <Th>Técnicas</Th>
                </Tr>
              </Thead>
              <Tbody>
                <Tr>
                  <Td key="key">Técnica #1</Td>
                </Tr>
              </Tbody>
            </Table>
          ) : null}
        </>
      ) : (
        <>
          <TrastornoEdit
            trastornoProp={trastorno}
            id={props.index}
            setEdit={setEdit}
            setTrast={setTrastorno}
          />
        </>
      )}
    </>
    
  )
};

export default TrastornoDetail;
