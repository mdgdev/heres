import { useState } from "react";
import {
  Input,
  Button,
  HStack,
  OrderedList,
  ListItem,
} from "@chakra-ui/react";

import {
  addSubCriterio,
  addTrastornoCriterio
} from '../../functions/TrastornoConfig'


// Formulario para los criterios

const CriterioForm = ({
  value,
  iter,
  trastornoCriterio,
  subCriterios,
  trastornoCriterios,
  setTrastornoCriterios,
}) => {
  const [posicion, setPosicion] = useState(subCriterios?.length || 0);

  let initialCriterio = [];
  if (subCriterios) {
    initialCriterio = trastornoCriterio.Subcriterios.map((criterio, index) => {
      return (
        <ListItem>
          <Input
            defaultValue={criterio}
            size="sm"
            mb="5px"
            bg="gray.50"
            onChange={({ target }) => addSubCriterio(target, iter, index, trastornoCriterios, setTrastornoCriterios)}
          />
        </ListItem>
      );
    });
  }

  const [subCrit, setSubCrit] = useState(initialCriterio);

  const addSub = () => {
    const newSubCrit = [...subCrit];
    const nuevaPosicion = posicion + 1;
    setPosicion(nuevaPosicion);
    newSubCrit.push(
      <ListItem key={nuevaPosicion}>
        <Input
          size="sm"
          mb="5px"
          bg="gray.50"
          onChange={({ target }) => addSubCriterio(target, iter, posicion, trastornoCriterios, setTrastornoCriterios)}
        />
      </ListItem>
    );
    setSubCrit(newSubCrit);
  };
  return (
    <>
      <HStack>
        <Input
          value={value}
          size="sm"
          onChange={({ target }) => addTrastornoCriterio(target, iter,  trastornoCriterios, setTrastornoCriterios)}
        />
        <Button size="sm" onClick={addSub} fontSize={13} pr={7} pl={7}>
          Añadir subcriterio
        </Button>
      </HStack>
      <OrderedList pl="15px">{subCrit.map((elem) => elem)}</OrderedList>
    </>
  );
};

export default CriterioForm;
