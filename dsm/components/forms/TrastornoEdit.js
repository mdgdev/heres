import { useState } from "react";
import CriterioForm from "./CriterioForm";
import trastornoServices from "../../services/trastornos";
import {
  Stack,
  Input,
  Text,
  Textarea,
  Button,
  Select,
  Grid,
  Box,
} from "@chakra-ui/react";
import useCategorias from "../../hooks/useCategorias";
import useTrastornos from "../../hooks/useTrastornos";
import useCriterios from "../../hooks/useCriterios";
import styles from "./TrastornoEdit.module.css";

/* Begin code
 * Redesign of Formulario (0.2.3)

*/

const TrastornoEdit = ({ trastornoProp, id, setEdit, setTrast }) => {
  // ------ Custom Hooks
  const trastornoHook = useTrastornos(trastornoProp);
  const categorias = useCategorias([]);
  const criteriosNum = useCriterios(
    trastornoHook.trastornos.Criterios.length - 1
  );
  // --------------------

  const [tipos, setTipos] = useState([]);
  const [tipoSelected, setTipoSelected] = useState(
    trastornoHook.trastornos.Tipo
  );
  const [categoriaSelected, setCategoriaSelected] = useState(
    trastornoHook.trastornos.Categoria
  );
  const [isLoading, setIsLoading] = useState(false);

  // Hooks para el objeto Trastorno

  const [trastornoCriterios, setTrastornoCriterios] = useState(
    trastornoHook.trastornos.Criterios
  );

  const selectCategoria = (e) => {
    setCategoriaSelected("");
    trastornoHook.changeTrastornos(e.target.value, "Categoria");
    const categoriaEncontrada = categorias.filter(
      (categoria) => categoria.Nombre === e.target.value
    );

    if (categoriaEncontrada[0] && categoriaEncontrada[0].Tipos) {
      setTipos(categoriaEncontrada[0].Tipos);
      setTipoSelected("");
    }
  };

  const criterios = [];

  for (let i = 0; i <= criteriosNum.criterios; i++) {
    if (trastornoCriterios[i]) {
      criterios.push(
        <CriterioForm
          value={trastornoCriterios[i].Descripcion}
          iter={i}
          trastornoCriterio={trastornoCriterios[i]}
          subCriterios={trastornoHook.trastornos.Criterios[i]?.Subcriterios}
          trastornoCriterios={trastornoCriterios}
          setTrastornoCriterios={setTrastornoCriterios}
        />
      );
    } else {
      criterios.push(
        <CriterioForm
          iter={i}
          trastornoCriterios={trastornoCriterios}
          setTrastornoCriterios={setTrastornoCriterios}
        />
      );
    }
  }

  const trastornoEdit = async (event) => {
    event.preventDefault();
    const nuevoTrastorno = {
      id: id,
      Trastorno: trastornoHook.trastornos.Trastorno,
      Codigo: trastornoHook.trastornos.Codigo,
      Indice: trastornoHook.trastornos.Indice,
      Categoria: trastornoHook.trastornos.Categoria,
      Tipo: trastornoHook.trastornos.Tipo,
      Descripcion: trastornoHook.trastornos.Descripcion,
      Criterios: trastornoCriterios,
    };
    console.log(nuevoTrastorno);
    setIsLoading(true);
    const updatedTrastorno = await trastornoServices.update(nuevoTrastorno);
    trastornoHook.updateTrastorno(updatedTrastorno);
    setTrast(updatedTrastorno);
    setIsLoading(false);
    setEdit(false);
  };

  return (
    <>
      <form onSubmit={trastornoEdit}>
        <Stack spacing={1}>
          <Text className={styles.formTitulo}>Trastorno:</Text>
          <Input
          boxShadow="2px 2px 3px 1px #EEEEEE"
            className={styles.formInput}
            focusBorderColor="teal.400"
            mb={3}
            defaultValue={trastornoHook.trastornos.Trastorno}
            onChange={({ target }) =>
              trastornoHook.changeTrastornos(target.value, "Trastorno")
            }
          />
          <Grid templateColumns="repeat(2, 1fr)" gap={6}>
            <Box>
              <Text className={styles.formTitulo}>Código CIE-9</Text>
              <Input
              boxShadow="2px 2px 3px 1px #EEEEEE"
                focusBorderColor="teal.400"
                className={styles.formInput}
                mb={3}
                defaultValue={trastornoHook.trastornos.Codigo}
                onChange={({ target }) =>
                  trastornoHook.changeTrastornos(target.value, "Codigo")
                }
              />
            </Box>
            <Box>
              <Text className={styles.formTitulo}>Código CIE-10</Text>
              <Input
              boxShadow="2px 2px 3px 1px #EEEEEE"
                focusBorderColor="teal.400"
                className={styles.formInput}
                mb={3}
                defaultValue={trastornoHook.trastornos.Indice}
                onChange={({ target }) =>
                  trastornoHook.changeTrastornos(target.value, "Indice")
                }
              />
            </Box>
          </Grid>
          <Text className={styles.formTitulo}>Categoría:</Text>
          <Select
          boxShadow="2px 2px 3px 1px #EEEEEE"
            mb={3}
            className={styles.formInput}
            placeholder={categoriaSelected}
            onChange={selectCategoria}
          >
            {categorias.map((categoria) => (
              <option key={categoria.Nombre} value={categoria.Nombre}>
                {categoria.Nombre}
              </option>
            ))}
          </Select>
          <Text className={styles.formTitulo}>Tipo:</Text>
          <Select
          boxShadow="2px 2px 3px 1px #EEEEEE"
            mb={3}
            className={styles.formInput}
            placeholder={tipoSelected}
            onChange={({ target }) =>
              trastornoHook.changeTrastornos(target.value, "Tipo")
            }
          >
            {tipos.map((tipo) => (
              <option key={tipo} value={tipo}>
                {tipo}
              </option>
            ))}
          </Select>
          <Text className={styles.formTitulo}>Descripción:</Text>
          <Textarea
          boxShadow="2px 2px 3px 1px #EEEEEE"
            className={styles.formInput}
            defaultValue={trastornoHook.trastornos.Descripcion}
            onChange={({ target }) =>
              trastornoHook.changeTrastornos(target.value, "Descripcion")
            }
            rows="5"
            mb={3}
          />
          <Text className={styles.formTitulo}>Criterios:</Text>
          <Box
            p={5}
            borderRadius={5}
            borderColor="teal.200"
            borderWidth={1}
            mb={5}
          >
            <Stack spacing={4}>{criterios}</Stack>
            <Button
              size="sm"
              colorScheme="teal"
              onClick={criteriosNum.addCriterio}
            >
              Añadir criterio
            </Button>
          </Box>
          <Text className={styles.formTitulo}>Técnicas:</Text>
          <Box
            mt={2}
            p={5}
            borderRadius={5}
            borderColor="teal.200"
            borderWidth={1}
          >
            <Button size="sm" colorScheme="teal">
              Añadir técnica
            </Button>
          </Box>
          <Box mt={20} color="white">
            .
          </Box>
          {isLoading ? (
            <Button isLoading type="submit">
              Guardar
            </Button>
          ) : (
            <Button type="submit" colorScheme="green">
              Guardar
            </Button>
          )}
        </Stack>
      </form>
    </>
  );
};

export default TrastornoEdit;
