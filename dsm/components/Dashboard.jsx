import { useEffect, useState } from "react";
import Trastorno from "./Trastorno";
import { Button, Input, Select, Grid, Box, Flex, Center } from "@chakra-ui/react";
import styles from "../styles/Home.module.css";
import Header from "./Header";
import CONST from "../resources/CONST";
import PageButtons from './PageButtons';

export default function Dashboard({ token, setToken, setPage }) {
  const [dsmList, setDsmList] = useState([]);
  const [categoria, setCategoria] = useState("");
  const [tipoSelected, setTipoSelected] = useState("");
  const [search, setSearch] = useState("");
  const [listaTipos, setListaTipos] = useState([]);
  const [searchCIE10, setSearchCIE10] = useState("");
  const [searchCIE9, setSearchCIE9] = useState("");

  const [pageNumber, setPageNumber] = useState(0);


  const usersPerPage = 8;
  const pagesVisited = pageNumber * usersPerPage;

  useEffect(() => {
    fetch("http://localhost:3001/api/dsm")
      .then((response) => response.json())
      .then((data) => {
        setDsmList(data);
      });
  }, []);

  const onChange = (e) => {
    setSearch(e.target.value.toLowerCase());
    setPageNumber(0);
  };

  const onChangeCIE10 = (e) => {
    setSearchCIE10(e.target.value.toLowerCase());
    setPageNumber(0);
  };

  const onChangeCIE9 = (e) => {
    setSearchCIE9(e.target.value.toLowerCase());
    setPageNumber(0);
  };

  const selectCategoria = (e) => {
    setTipoSelected("");
    setListaTipos([]);
    setCategoria(e.target.value);
    setPageNumber(0);
  };

  const selectTipo = (e) => {
    setTipoSelected(e.target.value);
    setPageNumber(0);
  };

  const categoriesList = (lista) => {
    const categorias = lista.map((elemento) => elemento.Categoria);
    const uniqueCategorias = new Set(categorias);
    const categoriasListadas = [...uniqueCategorias];
    return categoriasListadas.map((categoria) => (
      <option key={categoria}>{categoria}</option>
    ));
  };

  const listadoTipos = (categoria) => {
    let tipos = dsmList.reduce((accum, curr) => {
      if (curr.Categoria === categoria) {
        if (curr.Tipo !== "") accum.push(curr.Tipo);
      }
      return accum;
    }, []);
    const uniqueTipos = new Set(tipos);
    const tiposListados = [...uniqueTipos];
    return tiposListados.map((tipo) => (
      <option value={tipo} key={tipo}>
        {tipo}
      </option>
    ));
  };

  const resultado = dsmList
    .filter((busqueda) =>
      busqueda.Trastorno.toLowerCase().includes(search.toLowerCase())
    )
    .filter((busqueda) =>
      busqueda.Categoria.toLowerCase().includes(categoria.toLowerCase())
    )
    .filter((busqueda) =>
      busqueda.Tipo.toLowerCase().includes(tipoSelected.toLowerCase())
    )
    .filter((busqueda) =>
      busqueda.Codigo.toLowerCase().includes(searchCIE10.toLocaleLowerCase())
    )
    .filter((busqueda) =>
      busqueda.Indice.toLowerCase().includes(searchCIE9.toLowerCase())
    );


  const resultadoPag = resultado.slice(
    pagesVisited,
    pagesVisited + usersPerPage
  );



  return (
    <>
      <Box bgColor="#f5f6fa">
        {/* Header */}
        <Header setPage={setPage} token={token} setToken={setToken} />
        {/* Body */}
        <Box p="20px">
          <Box ml="25px">
            <Input
              onChange={onChange}
              type="text"
              placeholder="Buscar trastorno"
              h="45px"
              w="350px"
              bgColor="white"
              borderColor="teal.300"
              borderWidth="2px"
              mt="5px"
              mb="5px"
              textColor="blue.400"
              mr="15px"
            />

            <Input
              onChange={onChangeCIE10}
              type="text"
              placeholder="Código CIE-10"
              h="45px"
              w="165px"
              bgColor="white"
              borderColor="teal.300"
              borderWidth="2px"
              mt="5px"
              mb="5px"
              textColor="blue.400"
              mr="20px"
            />

            <Input
              onChange={onChangeCIE9}
              type="text"
              placeholder="Código CIE-9"
              h="45px"
              w="165px"
              bgColor="white"
              borderColor="teal.300"
              borderWidth="2px"
              mt="5px"
              mb="5px"
              textColor="blue.400"
            />
            <Flex>
              <Select
                onChange={selectCategoria}
                h="45px"
                w="350px"
                bgColor="white"
                borderColor="teal.300"
                borderWidth="2px"
                mt="5px"
                mr="15px"
                marginBottom="5px"
                textColor="teal.400"
              >
                <option value="" key="todas">
                  Todas las categorías
                </option>

                {categoriesList(dsmList)}
              </Select>
              <Select
                onChange={selectTipo}
                h="45px"
                w="350px"
                bgColor="white"
                borderColor="teal.300"
                borderWidth="2px"
                mt="5px"
                mb="5px"
                textColor="teal.400"
              >
                <option value="" defaultValue key="todas">
                  Todos los tipos
                </option>
                {categoria !== "" ? listadoTipos(categoria) : null}
              </Select>
            </Flex>
            <Box mt="5px" className={styles.search}>
              Total de trastornos encontrados:&nbsp;
              <strong>{resultado.length}</strong>
            </Box>
          </Box>

          <Box backgroundColor="#f5f6fa" p="20px">
            <Grid templateColumns="repeat(4, 1fr)" gap={5} m="5px">
              {resultadoPag.map((elemento) => (
                <Trastorno
                  trastorno={elemento.Trastorno}
                  codigo={elemento.Codigo}
                  indice={elemento.Indice}
                  categoria={elemento.Categoria}
                  tipo={elemento.Tipo}
                  id={elemento.id}
                  key={elemento.id}
                  token={token}
                />
              ))}
            </Grid>
            <Box mt={10}>
              {/* Pagination */}
              <Center>
               <PageButtons 
               total={resultado.length} 
               setPageNumber={setPageNumber}
               pageNumber={pageNumber} />
              </Center>
            </Box>
          </Box>
        </Box>
        {/* Footer */}
      </Box>
    </>
  );
}
