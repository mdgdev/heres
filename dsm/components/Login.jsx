import { useState, useEffect } from "react";
import { Link, Button, Text, Input, Center, Box, Image } from "@chakra-ui/react";
import Logo from "../recursos/logo.png";
import styles from './Login.module.css';
import loginService from '../services/login';
import Cookie from 'js-cookie';

const Login = ({ setToken, setPage }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [newToken, setNewToken] = useState();

  useEffect(() => {
    if(newToken){ Cookie.set('token', newToken) }
    
  },[newToken])

    const handleClick = async event => {
      event.preventDefault();
      try{
        const newUser = await loginService.login(email,password);
        setNewToken(newUser.token);
        setToken(newUser.token);
        setPage('Dashboard')
        
      }catch(err){
        console.log(err);
      }
    }

  return (
    <form onSubmit={handleClick} >
    <Box bg="#F5F5F5" h="100vh">
      <Center>
        <Box 
        bg="white"
        mt={40} 
        borderRadius={20} 
        height="500px" 
        width="500px"
        boxShadow='0px 5px 10px #DDDDDD'
        >
          <Box>
            <Center pt={10} pb={10}>
              <Image src={Logo} alt="logo" />
            </Center>
          </Box>
          <Center>
            <Box w="300px" pb={5}>
              <Text
              className={styles.inputHeader}
              >Correo electrónico</Text>
              <Input 
              borderWidth={1}
              borderColor='blue.200'
              className={styles.inputText}
              fontSize={20}
              textColor='gray.600'
              placeholder='Nombre de usuario...'
              onChange={({target})=> setEmail(target.value)}
              />
            </Box>
          </Center>
          <Center>
            <Box w="300px" pb={5}>
              <Text
              className={styles.inputHeader}
              >Contraseña</Text>
              <Input 
              type='password'
              borderWidth={1}
              borderColor='blue.200'
              className={styles.inputText}
              placeholder='Contraseña'
              fontSize={20}
              onChange={({target})=> setPassword(target.value)}
              />
            </Box>
          </Center>
          <Box 
          ml={100}
          mt={5}
          ><Button
          w='140px'
          colorScheme='teal'
          type='submit'
          >Acceder</Button>
              <Button
          w='140px'
          colorScheme='teal'
          variant="outline"
          onClick={()=>setPage('Dashboard')}
          ml={5}
          >Cancelar</Button>
            <Box
            mt={2}
            ><Link href='#'
            fontSize='sm'
            textColor='teal.600'
            >Recuperar contraseña</Link></Box>
            
          </Box>
        </Box>
      </Center>
    </Box>
    </form>
  );
};

export default Login;
