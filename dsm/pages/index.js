import { useEffect, useState } from "react";
import Dashboard from "../Components/Dashboard";
import Login from "../components/Login";
import Head from "next/head";
import CONST from "../resources/CONST";
import parseCookies from '../lib/parseCookies';

const Home = ({ initialToken }) => {
  const [ token, setToken ] = useState(initialToken);
  const [ page, setPage ] = useState('Dashboard');
  
  return (
    <>
      <Head>
        <title>MDG DSM5Tool version {CONST.version} </title>
      </Head>
      { page === 'Login' ? <Login setToken={setToken} setPage={setPage} /> : <Dashboard setPage={setPage} token={token} setToken={setToken} /> }
       
      

    </>
  );
}

Home.getInitialProps = ({req}) => {
  const cookies = parseCookies(req);
   return {
     initialToken: cookies.token
   }
}

export default Home;