import axios from 'axios'

const server = 'http://localhost:3001'
const baseUrl = '/api/usuarios/login'

const login = async (email, password) => {
  const response = await axios.post(server + baseUrl, {email, password})
  return response.data
}

export default { login }