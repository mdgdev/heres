import axios from 'axios';

const server = 'http://localhost:3001'
const baseUrl = '/api/dsm';

const getAll = async () => {
    const response = await axios.get(server + baseUrl);
    return response.data
}

const update = async (newObject) => {
    const putUrl = server + baseUrl + `/${newObject.id}`
    const response = await axios.put(putUrl, newObject);
    console.log('data: ', response.data)
    return response.data
}

const getTrastorno = async id => {
    const response = await axios.get(`${server}${baseUrl}/${id}`)
    return response.data
}

export default { getAll, update, getTrastorno }