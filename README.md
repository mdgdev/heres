# Proyecto Heres I

## EOS 
Version 0.1
- Users for AURA
- Trastornos for DSM5Tool 

## AURA
Version 0.1
- Login 
- Dashboard design base


## IRIS
Version 0.1.0
- Express installed
- Connection to MongoDB 

## DSM5Tool
Version 0.1 

- Main page completed.

### Pending 
- Criteria pattern in EOS.
- Modal design.

## Documentación
- [Proyecto](https://gitlab.com/mdgdev/heres/-/blob/master/docs/projectoverview.md)

## Información adicional
- [MDG DevLog](https://dev.mdgmedia.es)
## Notas

## Change Log 
- 1.2: DSM5Tool side project 
- 1.0: Documentación actualizada disponible en /docs 
