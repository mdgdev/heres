import { FC, useState } from "react"; 
import Head from 'next/head';
import { CSSReset } from "@chakra-ui/react";
import Landing from '../components/layouts/Landing';
import User from "../components/webparts/User";


// const breakpoints = ["360px", "768px", "1024px", "1440px"];

const Home: FC = () =>  {
  const [token, setToken] = useState('');


  return (
    <>
    <Head>
          <title>Heres I - Main site</title>
    </Head>
    <CSSReset /> 
    {!token ? 
      <div>
        <Landing setToken={setToken} />
      </div> 
      : 
      <div>        
        <User token={token}></User>
      </div>
    }
    </>
  );
}

export default Home; 