import { FC } from "react";
import Head from "next/head";
import { CSSReset } from "@chakra-ui/react";
import UserPanel from "../components/sections/UserPanel";
import DataEntry from "../components/webparts/DataEntry";
import { FormType } from "../types/global";

const test: FC = () => {
  return (
    <>
      <Head>
        <title>Heres I - Main site</title>
      </Head>
      <CSSReset />
      <UserPanel page="user" />
      <DataEntry question="Pregunta de test" type={FormType.Lista} />
      <DataEntry question="Pregunta de test 2" type={FormType.Pregunta} />
    </>
  );
};

export default test;
