export type UsuarioBD = {
    name: string;
    username: string;
    token: string;
}