import { FC } from "react";
import { Grid, Box, Center, Text, Link } from "@chakra-ui/react"
import { CalendarIcon } from '@chakra-ui/icons';

/* 
Section v: 0.1
---------------------
Sección básica estándar a la que agregar después cabecera y pie

*/

interface Props {
    page: string;
}

const UserPanel: FC<Props> = (Props) => {

    // const [view, setView] = useState("");
    const page = Props.page;

    return(
        <>
        <Box padding="50px">
            {page}
        <Grid templateColumns="repeat(3, 1fr)" gap={6} pb="20px">
            <Box w="100%" h="22" bg="blue.500" ><Link href="#">
                <Box w="100%" mt="10px" pt="5px"><Center>
                    <CalendarIcon w={10} h={10} color="white"/>
                    </Center>
                    </Box>
                <Box w="100%" mt="10px" pb="10px"><Center>
                    <Text color="white">Nueva entrada</Text>
                    </Center>
                </Box></Link>
                </Box>
            
            <Box w="100%" h="20" bg="teal.500" >Editar entradas</Box>
            <Box w="100%" h="20" bg="red.500" >Contacto profesional</Box>
        </Grid>
        <Grid templateColumns="repeat(3, 1fr)" gap={6} pb="10px">
            <Box w="100%" h="20" bg="yellow.500" >Boton 4</Box>
            <Box w="100%" h="20" bg="green.500" >Boton 5</Box>
            <Box w="100%" h="20" bg="purple.500" >Boton 6</Box>
        </Grid>


        </Box>

        </>
    )
}

export default UserPanel;