import { FC } from "react";
import { Center, Box, Flex, Heading, Button, Spacer } from "@chakra-ui/react";
import Logo from "../ui/Logo";

const Header: FC = (props) => {
    return(
        <Flex
            as="nav"
            align="center"
            justify="space-between"
            wrap="wrap"
            padding="1.0rem"
            bg="teal.500"
            color="white"
            {...props}
        >
            <Box p="1">
                <Flex>
                    <Logo />
                    <Center>
                        <Heading size="md" pl="10px">Heres I - System </Heading>
                    </Center>
                </Flex>
                
            </Box>
            <Spacer />
            <Box mr="10px">
                <Button colorScheme="teal" mr="4" size="sm" >
                Sign Up
                </Button>
                <Button colorScheme="teal" size="sm">Log in</Button>
            </Box>
        </Flex>

    );
};

export default Header;