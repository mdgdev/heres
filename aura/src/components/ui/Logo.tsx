import { FC } from "react";
import { Image, Center } from "@chakra-ui/react"

const Logo: FC = () => { 
    return(
        <>
        <Center> 
            <Image boxSize="50px" src="baselogo.png" alt="Logo"/>
        </Center>
        </>
        );
};

export default Logo;