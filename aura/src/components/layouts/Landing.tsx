import { FC, SetStateAction } from "react";
import Header from "../../components/sections/Header";
import Main from "../../components/webparts/Main"
import Footer from "../../components/sections/Footer";

type Props = {
    setToken: React.Dispatch<SetStateAction<string>>;
}

const Landing: FC<Props> = ({ setToken }) => {
    return(
    <>
        <Header/>
        <Main 
        title="Ahora puedes decidir"
        subtitle="ser feliz"
        image="imagen.jpg"
        setToken={setToken}
        />
        <Footer /> 
    </>
    );
};

export default Landing; 