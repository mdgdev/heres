import React, { FC, ReactElement, useState, SetStateAction } from "react";
import { Modal, ModalOverlay, ModalHeader, ModalContent, ModalCloseButton, ModalBody, ModalFooter, Button, useDisclosure, FormControl, FormLabel, Input } from "@chakra-ui/react";
import { apiBaseUrl } from '../../constants';
import axios from 'axios';
import { UsuarioBD } from '../../types/login';


// Create interface for props 

type Props = {
  setToken: React.Dispatch<SetStateAction<string>>;
}


const FormButton: FC<Props> = ({setToken}) => {
    const { isOpen, onOpen, onClose } = useDisclosure()

    const [user, setUser] = useState(''); 
    const [password, setPassword] = useState('');

    const handleSubmit = async (event: { preventDefault: () => void; }) => {
      event.preventDefault();
      try { 
        const { data: usuario }  = await axios.post<UsuarioBD>(`${apiBaseUrl}/login`, {
          username: user,
          password: password
        }) 
        setToken(usuario.token);
        alert(`token: ${usuario.token}`);
      }
      catch( error ) {
        console.log("Error: ", error);
      }    
    
    }

    return(
        <>
        <Button 
        borderRadius="8px"
        mt="10px"
        py="4"
        px="4"
        mr="10px"
        lineHeight="1"
        size="md"
        bgColor="teal.700"
        color="white"
        onClick={onOpen}>Acceder al sistema</Button>
        <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Acceso</ModalHeader>
          <ModalCloseButton />
          <form onSubmit={handleSubmit}>
            <ModalBody>
          <FormControl>
              <FormLabel>Nombre de usuario</FormLabel>
              <Input type="text" placeholder="nombre de usuario" onChange={ event => setUser(event.target.value)}/>
            </FormControl>

            <FormControl mt={4}>
              <FormLabel>Contraseña</FormLabel>
              <Input placeholder="Contraseña" type="password"  onChange={ event => setPassword(event.target.value)} />
            </FormControl>
            
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="teal" mr={3} type="submit" >
              Acceder
            </Button>
            <Button variant="ghost" onClick={onClose}>Cerrar</Button>
          </ModalFooter>
        </form>
        </ModalContent>
      </Modal>
    </>
    );
}

export default FormButton; 