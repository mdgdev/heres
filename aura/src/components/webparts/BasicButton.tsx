import { FC } from "react";
import { Link, Button } from "@chakra-ui/react";

interface buttonProps {
    label: String;
    link: String;
}

const BasicButton: FC<buttonProps> = ({ label, link }) => {
    return(
        <>
        <Link to={link} textDecoration="none">
         <Button
            mt="10px"
            py="4"
            px="4"
            lineHeight="1"
            size="md"
          >
            {label}
          </Button>
          </Link>
        </>
    )
}


export default BasicButton;