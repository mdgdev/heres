import {FC, SetStateAction } from "react";
import { Box, Flex, Image, Heading, Stack, Text } from "@chakra-ui/react";
import FormButton from './FormButton';
import BasicButton from './BasicButton';


interface MainProps {
    title: string;
    subtitle: string;
    image: string;
    setToken: React.Dispatch<SetStateAction<string>>;
}

const Main: FC<MainProps> = ({ title, subtitle, image, setToken, ...rest}) => {
    const green = {
        color: "#2C7A7B" 
    }
    return(
        <Flex
        align="center"
        justify={{ base: "center", md: "space-around", xl: "space-between"}}
        direction={{ base: "column-reverse", md:"row"}}
        wrap="nowrap"
        minH="70vh"
        px={8}
        mb={16}
        {...rest}
        >
            <Stack
            w={{ base: "80%", md: "40%"}}
            align={["center", "center", "flex-start", "flex-start"]}
            >
                <Heading
                    as="h1"
                    size="4xl"
                    fontWeight="bold"
                    color="primary.800"
                    textAlign={["center", "center", "left", "left"]}
                    >
                    {title} <span style={green}>{subtitle}</span>
                </Heading>
                <Text 
                    fontSize="3xl"
                    fontWeight="bold"
                    opacity="0.5"
                    color="teal.800">
                        Sin tener que preocuparte del cómo
                    </Text>
        <Flex
         minH="10vh"
         wrap="nowrap"
        >
          <FormButton setToken={setToken} />
          <BasicButton link="/informacion" label="Más información" />
        </Flex>
      </Stack>
      <Box w={{ base: "80%", sm: "60%", md: "50%" }} mb={{ base: 12, md: 1 }}>
        <Image src={image} size="100%"  />
      </Box>
    </Flex>

    )
}

export default Main;