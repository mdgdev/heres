import { FC } from "react";
import { FormType } from "../../types/global";
import CheckForm from "./formulario/CheckForm";
import ListaForm from "./formulario/ListaForm";
import PreguntaForm from "./formulario/PreguntaForm";

interface Props {
  question: string;
  type: FormType;
}

const DataEntry: FC<Props> = (Props) => {
    let view = <></>;

    switch (Props.type) {
        case FormType.Check: {
            view = <CheckForm />; 
            break;
        }
        case FormType.Lista: {
            view = <ListaForm />; 
            break;
        }
        case FormType.Pregunta: {
            view = <PreguntaForm />;    
            break;
        }
        default: {
            break;
        }
    }

  return (
    <>
      <h1>{Props.question}</h1>
      {view}
      
    </>
  );
};

export default DataEntry;
