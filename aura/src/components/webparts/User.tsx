import { FC, useState } from "react";
import {
  Link,
  Grid,
  GridItem,
  Heading,
  Box,
  VStack,
  StackDivider,
} from "@chakra-ui/react";
import Logo from "../../components/ui/Logo";
import styles from "./user.module.css";
import { ChatIcon, CalendarIcon, ViewIcon } from "@chakra-ui/icons";
import UserPanel from "../sections/UserPanel";

interface Props {
  token: string;
}

const User: FC<Props> = ({ token }) => {
  const [bg, setBg] = useState("teal.900");
  const [bg2, setBg2] = useState("teal.900");
  const [bg3, setBg3] = useState("teal.900");
  const [current, setCurrent] = useState("user");

  const dataEntry = () => {
    setCurrent("dataEntry");
  };

  return (
    <div>
      {token}
      <Grid
        templateRows="repeat(2, 1fr)"
        templateColumns="repeat(5, 1fr)"
        gap={0}
      >
        <GridItem rowSpan={2} colSpan={1} bg="teal.900" pt="10px" autoFlow>
          <Logo />
          <Heading as="h1" size="md" color="white" textAlign="center" pb="20px">
            Panel de Usuario
          </Heading>
          <VStack
            divider={<StackDivider borderColor="teal.800" />}
            spacing={0}
            align="stretch"
          >
            <Link style={{ textDecoration: "none " }}>
              <Box
                h="35px"
                pl="10px"
                pt="5px"
                color="white"
                bg={bg}
                onMouseOver={() => setBg("teal.700")}
                onMouseOut={() => setBg("teal.900")}
              >
                <ViewIcon color="white" mr="10px" />
                <span className={styles.styleList}>Perfil</span>
              </Box>
            </Link>
            <Link style={{ textDecoration: "none " }}>
              <Box
                h="35px"
                pl="10px"
                pt="5px"
                bg={bg2}
                onMouseOver={() => setBg2("teal.700")}
                onMouseOut={() => setBg2("teal.900")}
              >
                <ChatIcon color="white" mr="10px" />
                <span className={styles.styleList}>Contacto</span>
              </Box>
            </Link>
            <Link style={{ textDecoration: "none " }}>
              <Box
                h="35px"
                pl="10px"
                pt="5px"
                bg={bg3}
                onMouseOver={() => setBg3("teal.700")}
                onMouseOut={() => setBg3("teal.900")}
              >
                <CalendarIcon color="white" mr="10px" />
                <span className={styles.styleList}>Evolución</span>
              </Box>
            </Link>
            <Box h="35px" pl="10px" pt="5px"></Box>
          </VStack>
        </GridItem>
        <GridItem colSpan={2} bg="gray.100">
          <UserPanel page="user" />
        </GridItem>
        <GridItem colSpan={2} bg="gray.100">
          Últimas Noticias 2 boxes = Ultimas noticias / Ultimos mensajes
        </GridItem>
        <GridItem colSpan={4} bg="gray.100">
          Información de contacto{" "}
        </GridItem>
      </Grid>
    </div>
  );
};

export default User;
